# miRAW #

This repository contains the data associated to the paper entitled "miRAW: A deep learning Approach to predict miRNA targets by analyzing whole miRNA transcripts".
Only data generated in the project is available. Resources obtained from other locations should be downloaded from their original source.

### What is this repository for? ###

* Here you can find the data* generated and used in the paper entitled "miRAW: A deep learning Approach to predict miRNA targets by analyzing whole miRNA transcripts".
* Version 1.0

### How do I get the data? ###

* Data is compressed in a .7z file too big to be displayed in bitbucket website. To access the data you need to clone or fork this Git repository:
```
git clone https://app86@bitbucket.org/bipous/miraw_data.git
```

### Data Organization ###

* ConfFiles: Contains the configuration files required to replicate the experiments reported in the paper using miRAW. Note that some of the steps presented in the paper involve randomization procedures (e.g. generating of CrossValidation datasets), which will result different training and testing datasets, generating slightly different outcomes in the results.
* Data: Contains the data used to train and test miRAW's DNN and to test miRAW. It contains the list of positive and negative target sites, the list of positive and negative miRNA:mRNA targets, the testing dataset, etc. 
* Results: Contains the results of miRAW experiments: network training (CrossValidation results), evaluation of miRAW and evalution of other tools. 

* AdditionalScripts: Contains some scripts used to crossReference CLIP/CLASH data with TarBase and mirTarBase. This code is not considered as part of miRAW.

### External Data ###

External resources used can be obtained from the following sites:

* Ensembl release 87 <-- We used martview to obtain the 3'UTR of the genes	http://dec2016.archive.ensembl.org/biomart
* miRbase (r15, r19, r20 and r21) <-- Used to obtain miRNA trasncripts.		http://www.mirbase.org/
* mirTarBAse r6 <-- Experimentally verified miRNA:gene interactions 	http://mirtarbase.mbc.nctu.edu.tw/php/download.php?ver=6.1&opt=show
* DIANA TarBase v7 <-- Experimentally verified miRNA:gene interactions	http://diana.imis.athena-innovation.gr/DianaTools/index.php?r=site/index (Register and apply for bulk download)
* TargetScan <-- Putative target sites broadly conserved among species.		http://www.targetscan.org/cgi-bin/targetscan/data_download.cgi?db=vert_70
* CLASH data (Helwak) <-- miRNA binding sites 	http://www.sciencedirect.com/science/article/pii/S009286741300439X
* CLIP data (Grosswendt) <-- miRNA binding sites 	https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4181535/


### Who do I talk to? ###

* Albert Pla Planas: a.p.planas@medisin.uio.no / plaalbert@gmail.com
* Twitter: @planinsky